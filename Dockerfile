FROM mambaorg/micromamba

# Set user to root
USER root

WORKDIR /app


COPY environment.yml /app/environment.yml
RUN micromamba create -f environment.yml -y
COPY pyproject.toml /app/pyproject.toml
COPY poetry.lock /app/poetry.lock
RUN micromamba run -n example_env poetry install --with dev


COPY ./docs/my_research /app/docs/my_research
COPY ./docs/_quarto.yml /app/docs/_quarto.yml
COPY ./docs/index.qmd /app/docs/index.qmd
COPY ./docs/about.qmd /app/docs/about.qmd
COPY ./docs/styles.css /app/docs/styles.css
