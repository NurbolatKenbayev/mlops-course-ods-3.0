## Extensions for VSCode
Установил в VSCode все необходимые расширения:
- Code Spell Checker (Street Side Software, Spelling checker for source code);
- Russian - Code Spell Checker (Street Side Software);
- Python extension for Visual Studio Code (Microsoft);
- autoDocstring: VSCode Python Docstring Generator (Nils Werner);
- Jupyter (Microsoft);
- GitLens — Supercharge Git in VS Code (GitKraken);
- Ruff (Astral Software);
- Quarto (quarto.org).



# CI-CD pipeline


## DinD
Создал свой docker-образ (см. **Dockerfile**), и опубликовал его в своем gitlab docker registry.
На следующих стадиях CI-CD будет использоваться готовый построенный образ.
Он будет пересобираться заново только в том случае, когда произошли изменения в зависимостях (**poetry.lock, pyproject.toml, env.yml**),
в самом **Dockerfile**, либо в конфигурационном файле **.gitlab-ci.yml**.

В качестве базового образа используется **mambaorg/micromamba**. С помощью файла **environment.yml** micromamba устанавливает все необходимые зависимости (научные пакеты), в том числе пакетный менеджер **poetry**. Далее, poetry в активированной среде устанавливает зависимости, указанные в **poetry.lock** и **pyproject.toml**,
в том числе и development dependencies.


## Linting
После того, как наш docker-образ готов, начинается стадия линтеринга кода с использованием ruff.


## Сборка и публикация
Далее идет стадия сборки и публикации проекта (используя **poetry**) в виде PyPI пакета в gitlab pypi registry.
В данном случае идет сборка питоновского пакета "my_app", который печатает "hello, world!".


## Публикация на gitlab pages

### Разведочный анализ данных
Провел разведочный анализ на датасет https://www.kaggle.com/datasets/new-york-city/ny-2015-street-tree-census-tree-data?select=StreetTreeCensus2015TreesDataDictionary20161102

С помощью Quarto сначала перевел свой исследовательский ноутбук ".ipynb" в ".qmd"-формат, и затем
собрал документацию в виде веб-страницы (./docs/_site/index.html). Опубликовал ее на gitlab pages.
